from django.urls import path

from apps.library.views import BookView, download_file


app_name = 'book'

urlpatterns = [
    path('', BookView.as_view()),
    path('download', download_file, name='download')
]
