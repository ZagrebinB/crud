from django.db import models


class Book(models.Model):
    author = models.CharField(max_length=128, null=False, blank=False)
    genre = models.CharField(max_length=64, null=False, blank=False)
    year = models.PositiveSmallIntegerField(null=False, blank=False)
    title = models.CharField(max_length=128, null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
