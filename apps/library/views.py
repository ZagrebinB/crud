from django.views.generic import TemplateView
from djqscsv import render_to_csv_response

from apps.library.models import Book


class BookView(TemplateView):
    template_name = 'book.html'


def download_file(request):
    qs = Book.objects.all().values()
    return render_to_csv_response(qs)
