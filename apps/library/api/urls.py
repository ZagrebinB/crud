from django.urls import path

from apps.library.api.views import BookAPI

urlpatterns = [
    path('books/', BookAPI.as_view()),
    path('books/<int:pk>', BookAPI.as_view()),
]
