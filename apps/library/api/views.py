from rest_framework.generics import CreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView

from apps.library.api.serializers import BookSerializer
from apps.library.models import Book


class BookAPI(CreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView):
    serializer_class = BookSerializer
    queryset = Book.objects.all()

    def get(self, request, *args, **kwargs):
        if not kwargs.get('pk'):
            return self.list(request, *args, **kwargs)
        return super().get(request, *args, **kwargs)
