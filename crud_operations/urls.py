from django.urls import path, include

urlpatterns = [
    path('book/', include('apps.library.urls', namespace='book')),
    path('api/', include('apps.library.api.urls'))
]
